package com.example.cocktail.view.category

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.example.cocktail.R
import com.example.cocktail.ui.theme.CocktailTheme
import com.example.cocktail.view.DisplayIfLoading
import com.example.cocktail.viewmodel.CocktailViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass.
 * Use the [CategoryFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class CategoryFragment : Fragment() {
    private val cocktailViewModel by activityViewModels<CocktailViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return ComposeView(requireActivity()).apply {
            // Dispose of the Composition when the view's LifecycleOwner is destroyed
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state by cocktailViewModel.categoryState.collectAsState()
                cocktailViewModel.getCategories()
                CocktailTheme() {
                    // in compose world
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colorScheme.background
                    ) {
                        DisplayIfLoading(isLoading = state.isLoading)
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .background(Color.Yellow)
                                .border(5.dp, Color.Black),
                            horizontalArrangement = Arrangement.Center
                        ) {
                            Text(text = "Select a category!", fontSize = 40.sp)
                        }
                        LazyColumn(
                            modifier = Modifier
                                .padding(top = 50.dp)
                                .fillMaxWidth(),
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            items(state.categories) { category ->
                                Text(
                                    text = category.strCategory,
                                    fontSize = 30.sp,
                                    modifier = Modifier.clickable(onClick = {
                                        cocktailViewModel.getCategoryDrinks(category.strCategory)
                                        findNavController().navigate(R.id.categoryDrinkFragment)
                                    })
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}
