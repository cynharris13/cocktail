package com.example.cocktail.view.drink

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import coil.compose.AsyncImage
import com.example.cocktail.model.entity.Drink
import com.example.cocktail.ui.theme.CocktailTheme
import com.example.cocktail.view.DisplayIfLoading
import com.example.cocktail.viewmodel.CocktailViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * A simple [Fragment] subclass.
 * Use the [DrinkFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class DrinkFragment : Fragment() {
    private val cocktailViewModel by activityViewModels<CocktailViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return ComposeView(requireActivity()).apply {
            // Dispose of the Composition when the view's LifecycleOwner is destroyed
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                val state by cocktailViewModel.drinkState.collectAsState()
                CocktailTheme() {
                    Surface(
                        modifier = Modifier.fillMaxSize(),
                        color = MaterialTheme.colorScheme.background
                    ) {
                        DisplayIfLoading(isLoading = state.isLoading)
                        Row(
                            modifier = Modifier
                                .fillMaxWidth()
                                .background(Color.Green)
                                .border(5.dp, Color.Black),
                            horizontalArrangement = Arrangement.Center
                        ) {
                            Text(text = "Here are some recipes!", fontSize = 35.sp)
                        }
                        LazyColumn(
                            modifier = Modifier
                                .padding(top = 50.dp)
                                .fillMaxWidth(),
                            horizontalAlignment = Alignment.CenterHorizontally
                        ) {
                            items(state.drinks) { drink ->
                                DisplayDrink(drink = drink)
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun DisplayDrink(drink: Drink) {
    Column(modifier = Modifier.border(5.dp, Color.Black, shape = RectangleShape)) {
        Row() {
            AsyncImage(
                model = drink.strDrinkThumb,
                contentDescription = "An image of ${drink.strDrink}",
                alignment = Alignment.Center,
                modifier = Modifier.fillMaxWidth()
            )
        }
        Text(text = drink.strDrink, fontSize = 30.sp, modifier = Modifier.padding(10.dp))
        Text(text = drink.strCategory, fontSize = 20.sp, modifier = Modifier.padding(10.dp))
        Text(text = "Ingredients:", fontSize = 20.sp, modifier = Modifier.padding(start = 10.dp))
        for ((index, ingredient) in drink.strIngredients.withIndex()) {
            Text(
                text = if (index < drink.strMeasures.size) {
                    "$ingredient: ${drink.strMeasures[index]}"
                } else { ingredient },
                modifier = Modifier.padding(start = 10.dp)
            )
        }
        Text(text = drink.strInstructions, fontSize = 20.sp, modifier = Modifier.padding(10.dp))
    }
}
