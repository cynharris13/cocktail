package com.example.cocktail.view.drink

import com.example.cocktail.model.entity.Drink

/**
 * Drink screen state.
 *
 * @property isLoading
 * @property drinks
 * @property error
 * @constructor Create empty Drink screen state
 */
data class DrinkScreenState(
    val isLoading: Boolean = false,
    val drinks: List<Drink> = emptyList(),
    val error: String = ""
)
