package com.example.cocktail.view.categorydrink

import com.example.cocktail.model.entity.CategoryDrink

/**
 * Category drink screen state.
 *
 * @property isLoading
 * @property categoryDrinks
 * @property error
 * @constructor Create empty Category drink screen state
 */
data class CategoryDrinkScreenState(
    val isLoading: Boolean = false,
    val categoryDrinks: List<CategoryDrink> = emptyList(),
    val error: String = ""
)
