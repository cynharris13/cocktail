package com.example.cocktail.view

import androidx.fragment.app.FragmentActivity
import com.example.cocktail.R
import dagger.hilt.android.AndroidEntryPoint

/**
 * Main activity.
 *
 * @constructor Create empty Main activity
 */
@AndroidEntryPoint
class MainActivity : FragmentActivity(R.layout.main_activity)
