package com.example.cocktail.view.category

import com.example.cocktail.model.entity.Category

/**
 * Category screen state.
 *
 * @property isLoading
 * @property categories
 * @property error
 * @constructor Create empty Category screen state
 */
data class CategoryScreenState(
    val isLoading: Boolean = false,
    val categories: List<Category> = emptyList(),
    val error: String = ""
)
