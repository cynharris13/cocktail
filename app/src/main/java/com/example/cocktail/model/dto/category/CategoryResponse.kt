package com.example.cocktail.model.dto.category

import kotlinx.serialization.Serializable

@Serializable
data class CategoryResponse(
    val drinks: List<CategoryDTO> = listOf()
)
