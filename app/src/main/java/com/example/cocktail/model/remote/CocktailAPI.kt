package com.example.cocktail.model.remote

import com.example.cocktail.model.dto.category.CategoryResponse
import com.example.cocktail.model.dto.categorydrink.CategoryDrinkResponse
import com.example.cocktail.model.dto.drink.DrinkResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Cocktail a p i.
 *
 * @constructor Create empty Cocktail a p i
 */
interface CocktailAPI {
    @GET(CATEGORY_ENDPOINT)
    suspend fun getCategories(): Response<CategoryResponse>

    @GET(CATEGORY_DRINK_ENDPOINT)
    suspend fun getCategoryDrink(
        @Query(CATEGORY_DRINK_FETCHED) categoryName: String
    ): Response<CategoryDrinkResponse>

    @GET(DRINK_ENDPOINT)
    suspend fun getDrink(
        @Query(DRINK_FETCHED) drinkName: String
    ): Response<DrinkResponse>

    companion object {
        private const val DRINK_FETCHED = "s"
        private const val CATEGORY_DRINK_FETCHED = "c"
        private const val CATEGORY_ENDPOINT = "list.php?c=list"
        private const val CATEGORY_DRINK_ENDPOINT = "filter.php"
        private const val DRINK_ENDPOINT = "search.php"
    }
}
