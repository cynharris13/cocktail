package com.example.cocktail.model.dto.categorydrink

import kotlinx.serialization.Serializable

@Serializable
data class CategoryDrinkResponse(
    val drinks: List<CategoryDrinkDTO> = emptyList()
)
