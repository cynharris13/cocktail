package com.example.cocktail.model.entity

/**
 * Category drink.
 *
 * @property idDrink
 * @property strDrink
 * @property strDrinkThumb
 * @constructor Create empty Category drink
 */
data class CategoryDrink(
    val idDrink: String = "",
    val strDrink: String = "",
    val strDrinkThumb: String = ""
)
