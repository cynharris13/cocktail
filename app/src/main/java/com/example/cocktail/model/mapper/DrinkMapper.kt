package com.example.cocktail.model.mapper

import com.example.cocktail.model.dto.drink.DrinkDTO
import com.example.cocktail.model.entity.Drink

/**
 * Drink mapper.
 *
 * @constructor Create empty Drink mapper
 */
class DrinkMapper : Mapper<DrinkDTO, Drink> {
    override fun invoke(dto: DrinkDTO): Drink = with(dto) {
        Drink(
            dateModified,
            idDrink,
            strAlcoholic,
            strCategory,
            strCreativeCommonsConfirmed,
            strDrink,
            strDrinkAlternate,
            strDrinkThumb,
            strGlass,
            strIBA,
            strImageAttribution,
            strImageSource,
            listOf(
                strIngredient1 ?: "",
                strIngredient2 ?: "",
                strIngredient3 ?: "",
                strIngredient4 ?: "",
                strIngredient5 ?: "",
                strIngredient6 ?: "",
                strIngredient7 ?: "",
                strIngredient8 ?: "",
                strIngredient9 ?: "",
                strIngredient10 ?: "",
                strIngredient11 ?: "",
                strIngredient12 ?: "",
                strIngredient13 ?: "",
                strIngredient14 ?: "",
                strIngredient15 ?: ""
            ).filter { it.isNotEmpty() },
            strInstructions,
            strInstructionsDE ?: "",
            strInstructionsES,
            strInstructionsFR,
            strInstructionsIT ?: "",
            strInstructionsZHHANS,
            strInstructionsZHHANT,
            strTags,
            strVideo,
            listOf(
                strMeasure1,
                strMeasure2 ?: "",
                strMeasure3 ?: "",
                strMeasure4 ?: "",
                strMeasure5 ?: "",
                strMeasure6 ?: "",
                strMeasure7 ?: "",
                strMeasure8 ?: "",
                strMeasure9 ?: "",
                strMeasure10 ?: "",
                strMeasure11 ?: "",
                strMeasure12 ?: "",
                strMeasure13 ?: "",
                strMeasure14 ?: "",
                strMeasure15 ?: ""
            ).filter { it.isNotEmpty() }
        )
    }
}
