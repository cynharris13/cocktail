package com.example.cocktail.model.entity

/**
 * Drink.
 *
 * @property dateModified
 * @property idDrink
 * @property strAlcoholic
 * @property strCategory
 * @property strCreativeCommonsConfirmed
 * @property strDrink
 * @property strDrinkAlternate
 * @property strDrinkThumb
 * @property strGlass
 * @property strIBA
 * @property strImageAttribution
 * @property strImageSource
 * @property strIngredients
 * @property strInstructions
 * @property strInstructionsDE
 * @property strInstructionsES
 * @property strInstructionsFR
 * @property strInstructionsIT
 * @property strInstructionsZHHANS
 * @property strInstructionsZHHANT
 * @property strTags
 * @property strVideo
 * @property strMeasures
 * @constructor Create empty Drink
 */
data class Drink(
    val dateModified: String = "",
    val idDrink: String = "",
    val strAlcoholic: String = "",
    val strCategory: String = "",
    val strCreativeCommonsConfirmed: String = "",
    val strDrink: String = "",
    val strDrinkAlternate: String? = "",
    val strDrinkThumb: String = "",
    val strGlass: String = "",
    val strIBA: String = "",
    val strImageAttribution: String = "",
    val strImageSource: String = "",
    val strIngredients: List<String> = emptyList(),
    val strInstructions: String = "",
    val strInstructionsDE: String = "",
    val strInstructionsES: String? = "",
    val strInstructionsFR: String? = "",
    val strInstructionsIT: String = "",
    val strInstructionsZHHANS: String? = "",
    val strInstructionsZHHANT: String? = "",
    val strTags: String = "",
    val strVideo: String? = "",
    val strMeasures: List<String> = emptyList()
)
