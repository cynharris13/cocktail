package com.example.cocktail.model.mapper

import com.example.cocktail.model.dto.category.CategoryDTO
import com.example.cocktail.model.entity.Category

/**
 * Category mapper.
 *
 * @constructor Create empty Category mapper
 */
class CategoryMapper : Mapper<CategoryDTO, Category> {
    override fun invoke(dto: CategoryDTO): Category = with(dto) { Category(strCategory) }
}
