package com.example.cocktail.model.dto.category

import kotlinx.serialization.Serializable

@Serializable
data class CategoryDTO(
    val strCategory: String = ""
)
