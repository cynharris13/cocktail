package com.example.cocktail.model

import com.example.cocktail.model.dto.category.CategoryResponse
import com.example.cocktail.model.dto.categorydrink.CategoryDrinkResponse
import com.example.cocktail.model.dto.drink.DrinkResponse
import com.example.cocktail.model.entity.Category
import com.example.cocktail.model.entity.CategoryDrink
import com.example.cocktail.model.entity.Drink
import com.example.cocktail.model.mapper.CategoryDrinkMapper
import com.example.cocktail.model.mapper.CategoryMapper
import com.example.cocktail.model.mapper.DrinkMapper
import com.example.cocktail.model.remote.CocktailAPI
import retrofit2.Response
import javax.inject.Inject

/**
 * Cocktail repository.
 *
 * @property api
 * @constructor Create empty Cocktail repo
 */
class CocktailRepo @Inject constructor(
    private val api: CocktailAPI
) {

    val catMapper: CategoryMapper = CategoryMapper()
    val catDrinkMapper: CategoryDrinkMapper = CategoryDrinkMapper()
    val drinkMapper: DrinkMapper = DrinkMapper()

    /**
     * Get categories.
     *
     * @return
     */
    suspend fun getCategories(): List<Category> {
        val categoryResponse: Response<CategoryResponse> = api.getCategories()
        if (categoryResponse.isSuccessful) {
            val categories = categoryResponse.body() ?: CategoryResponse()
            return categories.drinks.map { catMapper(it) }
        } else { return emptyList() }
    }

    /**
     * Get category drinks.
     *
     * @param categoryName
     * @return
     */
    suspend fun getCategoryDrinks(categoryName: String): List<CategoryDrink> {
        val categoryDrinkResponse: Response<CategoryDrinkResponse> =
            api.getCategoryDrink(categoryName)
        if (categoryDrinkResponse.isSuccessful) {
            val categoryDrinks = categoryDrinkResponse.body() ?: CategoryDrinkResponse()
            return categoryDrinks.drinks.map { catDrinkMapper(it) }
        } else { return emptyList() }
    }

    /**
     * Get drinks.
     *
     * @param drinkName
     * @return
     */
    suspend fun getDrinks(drinkName: String): List<Drink> {
        val drinkResponse: Response<DrinkResponse> = api.getDrink(drinkName)
        if (drinkResponse.isSuccessful) {
            val drinks = drinkResponse.body() ?: DrinkResponse()
            return drinks.drinks.map { drinkMapper(it) }
        } else { return emptyList() }
    }
}
