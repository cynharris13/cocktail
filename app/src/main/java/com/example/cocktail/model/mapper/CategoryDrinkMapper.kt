package com.example.cocktail.model.mapper

import com.example.cocktail.model.dto.categorydrink.CategoryDrinkDTO
import com.example.cocktail.model.entity.CategoryDrink

/**
 * Category drink mapper.
 *
 * @constructor Create empty Category drink mapper
 */
class CategoryDrinkMapper : Mapper<CategoryDrinkDTO, CategoryDrink> {
    override fun invoke(dto: CategoryDrinkDTO): CategoryDrink = with(dto) {
        CategoryDrink(
            idDrink,
            strDrink,
            strDrinkThumb
        )
    }
}
