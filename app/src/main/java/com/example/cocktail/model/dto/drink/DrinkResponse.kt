package com.example.cocktail.model.dto.drink

import kotlinx.serialization.Serializable

@Serializable
data class DrinkResponse(
    val drinks: List<DrinkDTO> = emptyList()
)
