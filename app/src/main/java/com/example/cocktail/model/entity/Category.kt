package com.example.cocktail.model.entity

/**
 * Category.
 *
 * @property strCategory
 * @constructor Create empty Category
 */
data class Category(
    val strCategory: String = ""
)
