package com.example.cocktail.model.dto.categorydrink

import kotlinx.serialization.Serializable

@Serializable
data class CategoryDrinkDTO(
    val idDrink: String = "",
    val strDrink: String = "",
    val strDrinkThumb: String = ""
)
