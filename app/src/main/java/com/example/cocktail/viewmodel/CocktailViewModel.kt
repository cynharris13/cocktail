package com.example.cocktail.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktail.model.CocktailRepo
import com.example.cocktail.view.category.CategoryScreenState
import com.example.cocktail.view.categorydrink.CategoryDrinkScreenState
import com.example.cocktail.view.drink.DrinkScreenState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Category drink view model.
 *
 * @property repo
 * @constructor Create empty Category drink view model
 */
@HiltViewModel
class CocktailViewModel @Inject constructor(private val repo: CocktailRepo) : ViewModel() {
    private val _categoryDrinks: MutableStateFlow<CategoryDrinkScreenState> =
        MutableStateFlow(CategoryDrinkScreenState())
    val categoryDrinkState: StateFlow<CategoryDrinkScreenState> get() = _categoryDrinks
    private val _categories: MutableStateFlow<CategoryScreenState> =
        MutableStateFlow(CategoryScreenState())
    val categoryState: StateFlow<CategoryScreenState> get() = _categories
    private val _drinks: MutableStateFlow<DrinkScreenState> =
        MutableStateFlow(DrinkScreenState())
    val drinkState: StateFlow<DrinkScreenState> get() = _drinks

    /**
     * Get characters.
     *
     */
    fun getCategoryDrinks(categoryName: String) = viewModelScope.launch {
        _categoryDrinks.update { it.copy(isLoading = true) }
        val categoryDrinks = repo.getCategoryDrinks(categoryName)
        _categoryDrinks.update { it.copy(isLoading = false, categoryDrinks = categoryDrinks) }
    }

    /**
     * Get characters.
     *
     */
    fun getCategories() = viewModelScope.launch {
        _categories.update { it.copy(isLoading = true) }
        val categories = repo.getCategories()
        _categories.update { it.copy(isLoading = false, categories = categories) }
    }

    /**
     * Get characters.
     *
     */
    fun getDrinks(drinkName: String) = viewModelScope.launch {
        _drinks.update { it.copy(isLoading = true) }
        val drinks = repo.getDrinks(drinkName)
        _drinks.update { it.copy(isLoading = false, drinks = drinks) }
    }
}
