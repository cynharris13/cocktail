package com.example.cocktail.viewmodel

import com.example.cocktail.CoroutinesTestExtension
import com.example.cocktail.model.CocktailRepo
import com.example.cocktail.model.entity.Category
import com.example.cocktail.model.entity.CategoryDrink
import com.example.cocktail.model.entity.Drink
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
internal class ViewModelTests {

    @RegisterExtension
    val extension = CoroutinesTestExtension()
    val repo = mockk<CocktailRepo>()
    val cocktailViewModel: CocktailViewModel = CocktailViewModel(repo)

    @Test
    @DisplayName("Tests category drink state")
    fun testCategoryDrinkViewModel() = runTest(extension.dispatcher) {
        // given
        val expected = listOf(CategoryDrink())
        coEvery { repo.getCategoryDrinks("") } coAnswers { expected }
        // when
        cocktailViewModel.getCategoryDrinks("")
        // then
        Assertions.assertFalse(cocktailViewModel.categoryDrinkState.value.isLoading)
        Assertions.assertEquals(expected, cocktailViewModel.categoryDrinkState.value.categoryDrinks)
    }

    @Test
    @DisplayName("Tests category state")
    fun testCategoryViewModel() = runTest(extension.dispatcher) {
        // given
        val expected = listOf(Category())
        coEvery { repo.getCategories() } coAnswers { expected }
        // when
        cocktailViewModel.getCategories()
        // then
        Assertions.assertFalse(cocktailViewModel.categoryState.value.isLoading)
        Assertions.assertEquals(expected, cocktailViewModel.categoryState.value.categories)
    }

    @Test
    @DisplayName("Tests drink state")
    fun testDrinkViewModel() = runTest(extension.dispatcher) {
        // given
        val expected = listOf(Drink())
        coEvery { repo.getDrinks("") } coAnswers { expected }
        // when
        cocktailViewModel.getDrinks("")
        // then
        Assertions.assertFalse(cocktailViewModel.drinkState.value.isLoading)
        Assertions.assertEquals(expected, cocktailViewModel.drinkState.value.drinks)
    }
}
