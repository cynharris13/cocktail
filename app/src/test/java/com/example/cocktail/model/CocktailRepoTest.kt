package com.example.cocktail.model

import com.example.cocktail.CoroutinesTestExtension
import com.example.cocktail.model.dto.category.CategoryDTO
import com.example.cocktail.model.dto.category.CategoryResponse
import com.example.cocktail.model.dto.categorydrink.CategoryDrinkDTO
import com.example.cocktail.model.dto.categorydrink.CategoryDrinkResponse
import com.example.cocktail.model.dto.drink.DrinkDTO
import com.example.cocktail.model.dto.drink.DrinkResponse
import com.example.cocktail.model.entity.Category
import com.example.cocktail.model.entity.CategoryDrink
import com.example.cocktail.model.entity.Drink
import com.example.cocktail.model.remote.CocktailAPI
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
internal class CocktailRepoTest {
    @RegisterExtension
    val extension = CoroutinesTestExtension()
    val api = mockk<CocktailAPI>()
    val repo = CocktailRepo(api)

    @Test
    @DisplayName("Tests retrieving categories")
    fun testGetCategories() = runTest(extension.dispatcher) {
        // given
        val dtos = CategoryResponse(listOf(CategoryDTO("hi")))
        val expected = listOf(Category("hi"))
        coEvery { api.getCategories().isSuccessful } coAnswers { true }
        coEvery { api.getCategories().body() } coAnswers { dtos }

        // when
        val categories = repo.getCategories()

        // then
        Assertions.assertEquals(expected, categories)
    }

    @Test
    @DisplayName("Tests retrieving category drinks")
    fun testGetCategoryDrinks() = runTest(extension.dispatcher) {
        // given
        val dtos = CategoryDrinkResponse(listOf(CategoryDrinkDTO("hi")))
        val expected = listOf(CategoryDrink("hi"))
        coEvery { api.getCategoryDrink("bye").isSuccessful } coAnswers { true }
        coEvery { api.getCategoryDrink("bye").body() } coAnswers { dtos }

        // when
        val categories = repo.getCategoryDrinks("bye")

        // then
        Assertions.assertEquals(expected, categories)
    }

    @Test
    @DisplayName("Tests retrieving drinks")
    fun testGetDrinks() = runTest(extension.dispatcher) {
        // given
        val dtos = DrinkResponse(listOf(DrinkDTO("hi")))
        val expected = listOf(Drink("hi"))
        coEvery { api.getDrink("bye").isSuccessful } coAnswers { true }
        coEvery { api.getDrink("bye").body() } coAnswers { dtos }

        // when
        val categories = repo.getDrinks("bye")

        // then
        Assertions.assertEquals(expected, categories)
    }
}
